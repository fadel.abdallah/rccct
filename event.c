
#include<stdio.h>
#include<stdlib.h>
#include <string.h>


#include "event.h"


void get_events_from_file(Event *events, char* fileName, int *n)
{
    FILE *file = fopen(fileName, "r");  
    if(file == NULL) {
        perror("Unable to open file!");
        exit(1);
    }

    int row = 0;
    char line[4098];

    while (fgets(line, 4098, file))
    {
        if (row >= 1)
        {
            //char tmp[4098] = line;
            char *str;
            char* token;
            char* umask;
            char* name;
            char* description;
            char* counter;
            const char s[2] = "\t";
            
            token = strtok(line, s);
            umask = strtok(NULL, s);
            //strcpy(events[row-5].umask, token);
            name = strtok(NULL, s);
            description = strtok(NULL, s);
            counter = strtok(NULL, s);

            //strcpy(events[row-5].name, token);
            str = strchr (token, ',');
            if(str != NULL){
                char *token1;
                const char s1[2] = ",";
                token1 = strtok(token, s1);

                while (token1 != NULL) {
                    if(token1[0] == ' ')
                        strcpy(events[row - 1].code, token1 + 1);
                    else
                        strcpy(events[row - 1].code, token1);
                    strcpy(events[row - 1].umask, umask);
                    strcpy(events[row - 1].brief_description, description);
                    strcpy(events[row - 1].counter, counter);
                    strcpy(events[row++ - 1].name, name);
                    token1 = strtok(NULL, ",");
                    //printf("%s\t%s\t%s\n", events[row - 6].code, events[row - 6].umask, 
                    //                events[row - 6].name);
                }
                row--;
            } else {
                strcpy(events[row - 1].code, token);
                strcpy(events[row- 1].umask, umask);
                strcpy(events[row - 1].name, name);
                strcpy(events[row - 1].brief_description, description);
                strcpy(events[row - 1].counter, counter);
                events[row - 1].active = 1;
                //printf("%s\n", counter);
                //printf("%s\t%s\t%s\n", events[row - 5].code, events[row - 5].umask, 
                //                    events[row - 5].name);
            }

        }
        row++;
    }

    (*n) = row - 1;
    
    fclose(file);
}

void print_all_events(Event *events, int n)
{   printf("Index\tCode\tUmask\tName\n");
    for(int i = 0; i < n; i++) {
        printf("%d\t%s\t%s\t%s\n",i, events[i].code, events[i].umask, events[i].name);
    }
}