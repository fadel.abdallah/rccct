
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <inttypes.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>

#define _XOPEN_SOURCE 500
#include<unistd.h>

#include "rdmsr.h"




/* Number of decimal digits for a certain number of bits */
/* (int) ceil(log(2^n)/log(10)) */
int decdigits[] = {
	1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5,
	5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10, 10,
	10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 15,
	15, 15, 16, 16, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 19,
	20
};

#define mo_hex  0x01
#define mo_dec  0x02
#define mo_oct  0x03
#define mo_raw  0x04
#define mo_uns  0x05
#define mo_chx  0x06
#define mo_mask 0x0f
#define mo_fill 0x40
#define mo_c    0x80

unsigned int highbit = 63, lowbit = 0;
int mode = mo_dec;



/* filter out ".", "..", "microcode" in /dev/cpu */
int dir_filter_r(const struct dirent *dirp) {
	if (isdigit(dirp->d_name[0]))
		return 1;
	else
		return 0;
}


int open_msr(int core) {

	char msr_filename[BUFSIZ];
	int fd;

	sprintf(msr_filename, "/dev/cpu/%d/msr", core);
	fd = open(msr_filename, O_RDONLY);
	if ( fd < 0 ) {
		if ( errno == ENXIO ) {
			fprintf(stderr, "rdmsr: No CPU %d\n", core);
			exit(2);
		} else if ( errno == EIO ) {
			fprintf(stderr, "rdmsr: CPU %d doesn't support MSRs\n",
					core);
			exit(3);
		} else {
			perror("rdmsr:open");
			fprintf(stderr,"Trying to open %s\n",msr_filename);
			exit(127);
		}
	}

	return fd;
}

 
long long read_msr(int fd, uint32_t reg) {

	uint64_t data;

	if ( pread(fd, &data, sizeof data, reg) != sizeof data ) {
		perror("rdmsr:pread");
		exit(127);
	}

	return (long long)data;
}

uint64_t rdmsr_on_cpu(uint32_t reg, int cpu)
{
	uint64_t data;
	int fd;
	char *pat;
	int width;
	char msr_file_name[64];
	unsigned int bits;

	sprintf(msr_file_name, "/dev/cpu/%d/msr", cpu);
	fd = open(msr_file_name, O_RDONLY);
	if (fd < 0) {
		if (errno == ENXIO) {
			fprintf(stderr, "rdmsr: No CPU %d\n", cpu);
			exit(2);
		} else if (errno == EIO) {
			fprintf(stderr, "rdmsr: CPU %d doesn't support MSRs\n",
				cpu);
			exit(3);
		} else {
			perror("rdmsr: open");
			exit(127);
		}
	}

	if (pread(fd, &data, sizeof data, reg) != sizeof data) {
		if (errno == EIO) {
			fprintf(stderr, "rdmsr: CPU %d cannot read "
				"MSR 0x%08"PRIx32"\n",
				cpu, reg);
			exit(4);
		} else {
			perror("rdmsr: pread");
			exit(127);
		}
	}
	
	close(fd);	

	return data;
}

uint64_t * rdmsr_on_all_cpus(uint32_t reg, int *n)
{
	struct dirent **namelist;
	int dir_entries;
	uint64_t *tab;
	dir_entries = scandir("/dev/cpu", &namelist, dir_filter_r, 0);
	
	*n = dir_entries;
	tab = malloc(sizeof(uint64_t[dir_entries]));
	
	while (dir_entries--) {
		uint64_t value = rdmsr_on_cpu(reg, atoi(namelist[dir_entries]->d_name));
		*(tab + (7 - dir_entries)) = value;
		//printf("%d : %ld\n",atoi(namelist[dir_entries]->d_name), value);
		free(namelist[dir_entries]);
	}
	free(namelist);

	return tab;
}

void rdmsr_on_all_cpus_a(uint32_t reg, int n, uint64_t data[])
{	
	int cpu;
	int i = n;
	//printf("\nindex %ld\n", reg);
	while(i--) {
		cpu = n - (i + 1);
		data[cpu] = rdmsr_on_cpu(reg, cpu);
		//printf("%ld\t", data[index_event][cpu]);
	}

}


void rdmsr_on_all_cores_temperature(uint32_t reg_temp, uint32_t reg_therm, int numCores, 
												uint64_t data_temp[], uint64_t data_therm[])
{
	int cpu;
	int i = numCores;
	while (i--)
	{
		/* code */
		cpu = numCores - (i + 1);
		data_temp[cpu] = rdmsr_on_cpu(reg_temp, cpu);
		data_therm[cpu] = rdmsr_on_cpu(reg_therm, cpu);
	}
	
}


void rdmsr_on_all_cpus_events(uint32_t reg, int n, uint64_t data[][n], int index)
{	
	int cpu;
	int i = n;
	//printf("\nindex %ld\n", reg);
	while(i--) {
		cpu = n - (i + 1);
		data[index][cpu] = rdmsr_on_cpu(reg, cpu);
		//printf("%ld\t", data[index][cpu]);
	}
}

void rdmsr_on_all_cpus_fixed_counters(int numSMT, uint64_t data[][numSMT], int perfFixedCounters, int perfGeneralCounters)
{	
	int cpu;
	int i = numSMT;
	//printf("\nindex %ld\n", reg);
	while(i--) {
		cpu = numSMT - (i + 1);
		for (size_t j = 0; j < perfFixedCounters; j++)
		{
			data[j][cpu] = rdmsr_on_cpu(IA32_FIXED_CTR0 + j, cpu);
		}

		for (size_t c = 0; c < perfGeneralCounters; c++)
		{
			data[perfFixedCounters + c][cpu] = rdmsr_on_cpu(IA32_PMC0 + c, cpu);
		}
		//printf("%ld\t", data[index_event][cpu]);
	}
}