#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct CPUID
{
    /* data */
    uint32_t eax, ebx, ecx, edx;
} CPUID;


CPUID* cpuid(uint32_t leaf, uint32_t subleaf);


