

#include <stdlib.h>
#include <stdbool.h>

#include "topology.h"
#include "cacheinfo.h"


// Bit positions for data extractions
#define AVX_OR_HTT_POS 0x10000000 // bit 28
#define SSE_POS        0x02000000 // bit 25
#define SSE2_POS       0x04000000 // bit 26
#define SSE3_POS       0x00000001 // bit 1
#define SSSE3_POS      0x00000200 // bit 9
#define SSE41_POS      0x00080000 // bit 19
#define SSE42_POS      0x00100000 // bit 20
#define X2APIC_POS     0x00200000 // bit 21
#define AVX2_POS       0x00000020 // bit 5
#define BMI1_POS       0x00000008 // bit 3
#define BMI2_POS       0x00000100 // bit 9
#define ADX_POS        0x00080000 // bit 19
#define AVX512F_POS    0x00010000 // bit 16
#define AVX512PFI_POS  0x04000000 // bit 26
#define AVX512ERI_POS  0x08000000 // bit 27
#define AVX512CDI_POS  0x10000000 // bit 28 
#define SHA_POS        0x20000000 // bit 29 
    

/*
*   CPUInfo stores information about the detected system CPU.
*/
typedef struct CPUInfo
{
    char *vendorString;     // Raw vendor string
    char *brandName;        // Brand name reported by the CPU
    int stepping;           // CPU stepping number
    int model;              // CPU model number
    int family;             // CPU family number
    int processorType;      // Processor type
    int extendedModel;      // CPU extended model number
    int extendedFamily;     // CPU extended family number
    int displayFamily;      // CPU display family number
    int displayModel;       // CPU display model number 
    bool isHTT;             // CPU Hyper threaded 
    bool isSSE;             // CPU supports the SSE extensions ((Streaming SIMD Extensions)
    bool isSSE2;            // CPU supports the SSE2 extensions.
    bool isSSE3;            // CPU supports the SSE3 extensions (Streaming SIMD Extensions 3). A value of 1 indicates the processor supports this technology.
    bool isSSSE3;           // A value of 1 indicates the presence of the Supplemental Streaming SIMD Extensions 3 (SSSE3). 
                            // A value of 0 indicates the instruction extensions are not present in the processor.
    bool isSSE41;           // A value of 1 indicates that the processor supports SSE4.1.
    bool isSSE42;           // A value of 1 indicates that the processor supports SSE4.2.
    bool isX2APIC;          // A value of 1 indicates that the processor supports x2APIC feature.
    bool isAVX;             // A value of 1 indicates the processor supports the AVX instruction extensions.
    bool isAVX2;            // A value of 1 indicates the processor supports the AVX2 instruction extensions.
    bool isBMI1;            // Bit Manipulations Instruction Set 1
    bool isBMI2;            // Bit Manipulations Instruction Set 2
    bool isADX;             // Multi-Precision Add-Carry Instruction Extensions
    bool isAVX512F;         // 512-bit extensions to Advanced Vector Extensions Foundation
    bool isAVX512PFI;       // 512-bit extensions to Advanced Vector Extensions Prefetch Instructions
    bool isAVX512ERI;       // 512-bit extensions to Advanced Vector Extensions Exponential and Reciprocal Instructions
    bool isAVX512CDI;       // 512-bit extensions to Advanced Vector Extensions Conflict Detection Instructions
    bool isSHA;             // Secure Hash Algorithm
    int perfFixedCounters;  // Fixed-function performance-monitoring counters
    int perfGeneralCounters;// Programmable performance-monitoring counters 

    Topology *topology;
    CacheInfo *cacheinfo;


} CPUInfo;


CPUInfo* cpuinfo();

void printCPUInfo(CPUInfo cpuinfo);