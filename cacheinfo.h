

#ifndef CACHEINFO
#define CACHEINFO

#include<stdio.h>

/** brief Cache type. */
typedef enum hwloc_obj_cache_type_e {
  HWLOC_OBJ_CACHE_UNIFIED,      /**< \brief Unified cache. */
  HWLOC_OBJ_CACHE_DATA,         /**< \brief Data cache. */
  HWLOC_OBJ_CACHE_INSTRUCTION   /**< \brief Instruction cache (filtered out by default). */
} hwloc_obj_cache_type_t;

typedef struct CacheInfo
{
    /* data */
    hwloc_obj_cache_type_t type;
    unsigned level;
    unsigned nbthreads_sharing;
    unsigned cacheid;

    unsigned linesize;
    unsigned linepart;
    int inclusive;
    int ways;
    unsigned sets;
    unsigned long size;
} CacheInfo;


/* INPUT EAX = 04H: Returns Deterministic Cache Parameters for Each Level
*  When CPUID executes with EAX set to 04H and ECX contains an index value, the processor 
*  returns encoded data that describe a set of deterministic cache parameters (for the cache level associated with the input in ECX). Valid
*  index values start from 0.
*
*  Software can enumerate the deterministic cache parameters for each level of 
*  the cache hierarchy starting with an index value of 0, until the parameters report the 
*  value associated with the cache type field is 0. The architecturally defined fields
*  reported by deterministic cache parameters are documented in Table 3-8.
*
* This Cache Size in Bytes = (Ways + 1) * (Partitions + 1) * (Line_Size + 1) * (Sets + 1)
*                          = (EBX[31:22] + 1) * (EBX[21:12] + 1) * (EBX[11:0] + 1) * (ECX + 1)
*
*  Reference: Intel® 64 and IA-32 Architectures Software Developer’s Manual Volume 2 (2A, 2B, 2C & 2D): Instruction Set Reference, A-Z
*/
CacheInfo * read_intel_caches(int *cachenum);
void print_intel_caches(CacheInfo *cacheinfo, int cachenum);


#endif
