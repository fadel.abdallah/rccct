

#ifndef RDMSR
#define RDMSR

#include <dirent.h>

#include "intelmsr.h"

int dir_filter_r(const struct dirent *dirp);



int open_msr(int core);

long long read_msr(int fd, uint32_t reg);

uint64_t rdmsr_on_cpu(uint32_t reg, int cpu);
uint64_t * rdmsr_on_all_cpus(uint32_t reg, int *n);
void rdmsr_on_all_cpus_a(uint32_t reg, int n, uint64_t data[]);
void rdmsr_on_all_cores_temperature(uint32_t reg_temp, uint32_t reg_therm, int numCores, uint64_t data_temp[], uint64_t data_therm[]);
void rdmsr_on_all_cpus_events(uint32_t reg, int n, uint64_t data[][n], int index);
void rdmsr_on_all_cpus_fixed_counters(int numSMT, uint64_t data[][numSMT], int perfFixedCounters, int perfGeneralCounters);
#endif