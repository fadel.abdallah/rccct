CC = gcc
PWD ?= $(shell pwd)
KERNELDIR ?= /lib/modules/$(shell uname -r)/build
PREFIX ?= /usr


all: RCCCT
	$(MAKE) -j1 -C $(KERNELDIR) M=$(PWD) modules

install: module-install

module-install:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules_install


clean:
	rm -f m.o  RCCCT RCCCT.o event.o cpuinfo.o rapl.o
	#$(MAKE) -j1 -C $(KERNELDIR) M=$(PWD) clean

event.o:
	$(CC) -Wall -c event.c -o event.o

cpuinfo.o:
	$(CC) -Wall -c cpuinfo.c -o cpuinfo.o

cacheinfo.o:
	$(CC) -Wall -c cacheinfo.c -o cacheinfo.o

topology.o:
	$(CC) -Wall -c topology.c  -o topology.o -D_GNU_SOURCE

cpuid.o:
	$(CC) -Wall -c cpuid.c -o cpuid.o

rapl.o:
	$(CC) -Wall -c rapl.c -o rapl.o

rdmsr.o:
	$(CC) -Wall -c rdmsr.c -o rdmsr.o 

wrmsr.o:
	$(CC) -Wall -c wrmsr.c -o wrmsr.o

RCCCT.o:
	$(CC) -Wall -c RCCCT.c -o RCCCT.o

RCCCT: RCCCT.o event.o cpuinfo.o cacheinfo.o cpuid.o topology.o rapl.o rdmsr.o wrmsr.o
	$(CC) -o RCCCT RCCCT.o event.o cpuinfo.o cacheinfo.o cpuid.o topology.o rapl.o rdmsr.o wrmsr.o -lm

