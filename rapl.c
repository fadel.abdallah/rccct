
#include <stdio.h>
#include <linux/version.h>
#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/pci.h>
#ifdef CONFIG_DMI
#include <linux/dmi.h>
#endif /* CONFIG_DMI */
#include <linux/fs.h>

#include <linux/utsname.h>
#ifdef CONFIG_CPU_IDLE
#include <linux/cpuidle.h>
#endif /* CONFIG_CPU_IDLE */
#ifdef CONFIG_CPU_FREQ
#include <linux/cpufreq.h>
#endif /* CONFIG_CPU_FREQ */
#include <inttypes.h>

#include <asm/msr.h>


#include <math.h>
#include <string.h>

#include "rapl.h"


#include "rdmsr.h"


#include "intelmsr.h"

void closef(RAPL **rapl) {
    close((*rapl)->fd);  
}

RAPL rapl_init1(int cpu)
{
    RAPL rapl;
    //rapl = malloc(sizeof(struct RAPL));

    //uint64_t t = rdmsr(0x606);
    rapl.fd = open_msr(cpu);
    //printf("c %d\n", (rapl).fd);
    rapl.pp1_supported = detect_pp1();

    /* Read MSR_RAPL_POWER_UNIT Register */
    uint64_t raw_value = read_msr(rapl.fd, MSR_RAPL_POWER_UNIT);
    rapl.power_units = pow(0.5, (double) (raw_value & 0xf));
    rapl.energy_units = pow(0.5, (double) ((raw_value >> 8) & 0x1f));
    rapl.time_units = pow(0.5, (double) ((raw_value >> 16) & 0xf));
    
    
    return rapl;
}

RAPL * rapl_init(int cpu)
{
    RAPL *rapl;
    rapl = malloc(sizeof(struct RAPL));

    if(rapl == NULL) {
        printf("Error ..\n");
        exit(0);
    }

    //uint64_t t = rdmsr(0x606);
    rapl->fd = open_msr(cpu);
    rapl->pp1_supported = detect_pp1();

    /* Read MSR_RAPL_POWER_UNIT Register */
    uint64_t raw_value = read_msr(rapl->fd, MSR_RAPL_POWER_UNIT);
    rapl->power_units = pow(0.5, (double) (raw_value & 0xf));
    rapl->energy_units = pow(0.5, (double) ((raw_value >> 8) & 0x1f));
    rapl->time_units = pow(0.5, (double) ((raw_value >> 16) & 0xf));
   

    reset(rapl);



    return rapl;
}


void reset(RAPL *rapl)
{
    memset(&rapl->state1, 0, sizeof(rapl_state_t));
    memset(&rapl->state2, 0, sizeof(rapl_state_t));
    memset(&rapl->state3, 0, sizeof(rapl_state_t));
    rapl->prev_state = &rapl->state1;
	rapl->current_state = &rapl->state2;
	rapl->next_state = &rapl->state3;
    
	// sample twice to fill current and previous
	sample_rapl(&rapl);
	sample_rapl(&rapl);
  
	// Initialize running_total
	rapl->running_total.pkg = 0;
	rapl->running_total.pp0 = 0;
    rapl->running_total.pp1 = 0;
	rapl->running_total.dram = 0;
	//gettimeofday(&(rapl->running_total.tsc), NULL);
    rapl->running_total.tsc = rapl->current_state->tsc;
}

void sample_rapl(RAPL **rapl)
{
    
    //RAPL *rapl1 = &(*rapl);
    //printf("supp %d\n", (*rapl)->pp1_supported);
    uint32_t max_int = ~((uint32_t) 0);

   
	(*rapl)->next_state->pkg = read_msr((*rapl)->fd, MSR_PKG_ENERGY_STATUS) & max_int;
	(*rapl)->next_state->pp0 = read_msr((*rapl)->fd, MSR_PP0_ENERGY_STATUS) & max_int;
    
	if ((*rapl)->pp1_supported) {
		(*rapl)->next_state->pp1 = read_msr((*rapl)->fd, MSR_PP1_ENERGY_STATUS) & max_int;
		//(*rapl)->next_state->dram = 0;
        (*rapl)->next_state->dram = read_msr((*rapl)->fd, MSR_DRAM_ENERGY_STATUS) & max_int;
	} else {
		(*rapl)->next_state->pp1 = read_msr((*rapl)->fd, MSR_PP1_ENERGY_STATUS) & max_int;
        //(*rapl)->next_state->pp1 = 0;
		(*rapl)->next_state->dram = read_msr((*rapl)->fd, MSR_DRAM_ENERGY_STATUS) & max_int;
	}

	gettimeofday(&((*rapl)->next_state->tsc), NULL);
   
     


	// Update running total
	(*rapl)->running_total.pkg += energy_delta((*rapl)->current_state->pkg, (*rapl)->next_state->pkg);
	(*rapl)->running_total.pp0 += energy_delta((*rapl)->current_state->pp0, (*rapl)->next_state->pp0);
	(*rapl)->running_total.pp1 += energy_delta((*rapl)->current_state->pp1, (*rapl)->next_state->pp1);
	(*rapl)->running_total.dram += energy_delta((*rapl)->current_state->dram, (*rapl)->next_state->dram);
    //  printf("ok1\n");
	// Rotate states
	rapl_state_t *pprev_state = (*rapl)->prev_state;
	(*rapl)->prev_state = (*rapl)->current_state;
	(*rapl)->current_state = (*rapl)->next_state;
	(*rapl)->next_state = pprev_state;
}


double pkg_current_power(RAPL rapl) {
	double t = time_delta(&(rapl.prev_state->tsc), &(rapl.current_state->tsc));
	return power(rapl.prev_state->pkg, rapl.current_state->pkg, t, rapl.energy_units);
}

double pp0_current_power(RAPL rapl) {
	double t = time_delta(&(rapl.prev_state->tsc), &(rapl.current_state->tsc));
	return power(rapl.prev_state->pp0, rapl.current_state->pp0, t, rapl.energy_units);
}

double pp1_current_power(RAPL rapl) {
	double t = time_delta(&(rapl.prev_state->tsc), &(rapl.current_state->tsc));
	return power(rapl.prev_state->pp1, rapl.current_state->pp1, t, rapl.energy_units);
}

double dram_current_power(RAPL rapl) {
	double t = time_delta(&(rapl.prev_state->tsc), &(rapl.current_state->tsc));
	return power(rapl.prev_state->dram, rapl.current_state->dram, t, rapl.energy_units);
}

//
double pkg_average_power(RAPL *rapl) {
	return pkg_total_energy(rapl) / total_time(rapl);
}

double pp0_average_power(RAPL *rapl) {
    return pp0_total_energy(rapl) / total_time(rapl);
}

double pp1_average_power(RAPL *rapl) {
    return pp1_total_energy(rapl) / total_time(rapl);
}

double dram_average_power(RAPL *rapl) {
    return dram_total_energy(rapl) / total_time(rapl);
}

double pkg_total_energy(RAPL *rapl) {
	return rapl->energy_units * ((double) rapl->running_total.pkg);
}

double pp0_total_energy(RAPL *rapl) {
    return rapl->energy_units * ((double) rapl->running_total.pp0);
}

double pp1_total_energy(RAPL *rapl) {
    return rapl->energy_units * ((double) rapl->running_total.pp1);
}

double dram_total_energy(RAPL *rapl) {
    return rapl->energy_units * ((double) rapl->running_total.dram);
}

double total_time(RAPL *rapl) {
	return time_delta(&(rapl->running_total.tsc), &(rapl->current_state->tsc));
}

// double current_time(struct timeval *prev_state, struct timeval *current_state) {
// 	return time_delta(&(prev_state->tsc), &(current_state->tsc));
// }


// A voir

void sample_start(RAPL *rapl)
{
    uint32_t max_int = ~((uint32_t) 0);
    /* Read MSR_PKG_ENERGY_STATUS register (PKG) */
    rapl->state_start.pkg = read_msr(rapl->fd, MSR_PKG_ENERGY_STATUS) & max_int;
    //printf("pkg %" PRIu64 "\n", rapl->state_start.pkg);
    //sleep(1);
    //printf("POWER %f\n", (read_msr(rapl->fd, 0x611) - rapl->state_start.pkg) * rapl->energy_units);


    /* Read MSR_PP0_ENERGY_STATUS register (PP0) */
    rapl->state_start.pp0 = read_msr(rapl->fd, MSR_PP0_ENERGY_STATUS) & max_int;


    if(rapl->pp1_supported) {
        /* case PP1 is Supported */
        /* Read MSR_PP0_ENERGY_STATUS register (PP1) */
        rapl->state_start.pp1 = read_msr(rapl->fd, MSR_PP1_ENERGY_STATUS) & max_int;
        rapl->state_start.dram = 0;
    } else {
        /* case PP1 is Not Supported */
        /* Read MSR_DRAM_ENERGY_STATUS register (DRAM) */
        rapl->state_start.pp1 = 0;
        rapl->state_start.dram = read_msr(rapl->fd, MSR_DRAM_ENERGY_STATUS) & max_int;
    }

    
    gettimeofday(&(rapl->state_start.tsc), NULL);

}


void sample_end(RAPL *rapl)
{ 
    uint32_t max_int = ~((uint32_t) 0);
    /* Read MSR_PKG_ENERGY_STATUS register (PKG) */
    rapl->state_end.pkg = read_msr(rapl->fd, MSR_PKG_ENERGY_STATUS) & max_int;
    //printf("pkg %" PRIu64 "\n", rapl->state_start.pkg);
    //sleep(1);
    //printf("POWER %f\n", (read_msr(rapl->fd, 0x611) - rapl->state_start.pkg) * rapl->energy_units);

    /* Read MSR_PP0_ENERGY_STATUS register (PP0) */
    rapl->state_end.pp0 = read_msr(rapl->fd, MSR_PP0_ENERGY_STATUS) & max_int;

    if(rapl->pp1_supported) {
        /* case PP1 is Supported */
        /* Read MSR_PP0_ENERGY_STATUS register (PP1) */
        rapl->state_end.pp1 = read_msr(rapl->fd, MSR_PP1_ENERGY_STATUS) & max_int;
        rapl->state_end.dram = 0;
    } else {
        /* case PP1 is Not Supported */
        /* Read MSR_DRAM_ENERGY_STATUS register (DRAM) */
        rapl->state_end.pp1 = 0;
        rapl->state_end.dram = read_msr(rapl->fd, MSR_DRAM_ENERGY_STATUS) & max_int;
    }

    gettimeofday(&(rapl->state_end.tsc), NULL);
   
}


double time_delta(struct timeval *start, struct timeval *end) {
        return (end->tv_sec - start->tv_sec)
                + ((end->tv_usec - start->tv_usec)/1000000.0);
}

uint64_t energy_delta(uint64_t start, uint64_t end) {
	uint64_t max_int = ~((uint32_t) 0);
	uint64_t eng_delta = end - start;

	// Check for rollovers
	if (start > end) {
		eng_delta = end + (max_int - start);
	}
   
	return eng_delta;
}

double power(uint64_t start, uint64_t end, double time_delta, double energy_units) {
	if (time_delta == 0.0f || time_delta == -0.0f) { return 0.0; }
	double energy = energy_units * ((double) energy_delta(start, end));
    //printf("eEnergy %f \n", energy);
	return energy / time_delta;
}


double pkg_power(RAPL rapl){
    double t = time_delta(&(rapl.state_start.tsc), &(rapl.state_end.tsc));
    return power(rapl.state_start.pkg, rapl.state_end.pkg, t, rapl.energy_units);
}

double pp0_power(RAPL rapl){
    double t = time_delta(&(rapl.state_start.tsc), &(rapl.state_end.tsc));
    return power(rapl.state_start.pp0, rapl.state_end.pp0, t, rapl.energy_units);
}

double pp1_power(RAPL rapl){
    double t = time_delta(&(rapl.state_start.tsc), &(rapl.state_end.tsc));
    return power(rapl.state_start.pp1, rapl.state_end.pp1, t, rapl.energy_units);
}

double dram_power(RAPL rapl){
    double t = time_delta(&(rapl.state_start.tsc), &(rapl.state_end.tsc));
    return power(rapl.state_start.dram, rapl.state_end.dram, t, rapl.energy_units);
}


double pkg_energy(RAPL rapl)
{
    return rapl.energy_units * ((double) energy_delta(rapl.state_start.pkg, rapl.state_end.pkg));
}


double pp0_energy(RAPL rapl)
{
    return rapl.energy_units * ((double) energy_delta(rapl.state_start.pp0, rapl.state_end.pp0));
}


double pp1_energy(RAPL rapl)
{
    return rapl.energy_units * ((double) energy_delta(rapl.state_start.pp1, rapl.state_end.pp1));
}

double dram_energy(RAPL rapl)
{
    return rapl.energy_units * ((double) energy_delta(rapl.state_start.dram, rapl.state_end.dram));
}




#define SIGNATURE_MASK                 0xFFFF0
#define IVYBRIDGE_E                    0x306F0
#define SANDYBRIDGE_E                  0x206D0

int detect_pp1() {
	uint32_t eax_input = 1;
	uint32_t eax;
	__asm__("cpuid;"
			:"=a"(eax)               // EAX into b (output)
			:"0"(eax_input)          // 1 into EAX (input)
			:"%ebx","%ecx","%edx");  // clobbered registers

	uint32_t cpu_signature = eax & SIGNATURE_MASK;
	//printf("%x\n", cpu_signature);
    if (cpu_signature == SANDYBRIDGE_E || cpu_signature == IVYBRIDGE_E) {
		return 0;
	}
	
    return 1;
}