
#ifndef RAPL
#define RAPL_H_


#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>

#ifndef _XOPEN_SOURCE
    #define _XOPEN_SOURCE 500
#endif
#include<unistd.h>

//#define MAX_CPUS	    1024
//#define MAX_PACKAGES	16


typedef struct rapl_state_t
{
    uint64_t pkg;
    uint64_t pp0;
    uint64_t pp1;
    uint64_t dram;
    struct timeval tsc;
} rapl_state_t;


typedef struct RAPL
{
    // RAPL configuration
    int core;
    int fd;
    int pp1_supported;
    double power_units, energy_units, time_units;
    double thermal_spec_power, minimum_power, maximum_power, time_window;


    // RAPL state
    rapl_state_t state_start, state_end;
    // Rapl state
	rapl_state_t *current_state;
	rapl_state_t *prev_state;
	rapl_state_t *next_state;
	rapl_state_t state1, state2, state3, running_total;
} RAPL;

RAPL rapl1;

RAPL rapl_init1(int cpu);
RAPL * rapl_init(int cpu);
void reset(RAPL *rapl);
void sample_rapl(RAPL **rapl);
void sample_start(RAPL *rapl);
void sample_end(RAPL *rapl);



double time_delta(struct timeval *start, struct timeval *end);
uint64_t energy_delta(uint64_t start, uint64_t end);
double power(uint64_t start, uint64_t end, double time_delta, double energy_units);

double total_time(RAPL *rapl);



double pkg_current_power(RAPL rapl);
double pp0_current_power(RAPL rapl);
double pp1_current_power(RAPL rapl);
double dram_current_power(RAPL rapl);


double pkg_average_power(RAPL *rapl);
double pp0_average_power(RAPL *rapl);
double pp1_average_power(RAPL *rapl);
double dram_average_power(RAPL *rapl);

double pkg_total_energy(RAPL *rapl);
double pp0_total_energy(RAPL *rapl);
double pp1_total_energy(RAPL *rapl);
double dram_total_energy(RAPL *rapl);


double pkg_power(RAPL rapl);
double pp0_power(RAPL rapl);
double pp1_power(RAPL rapl);
double dram_power(RAPL rapl);

double pkg_energy(RAPL rapl);
double pp0_energy(RAPL rapl);
double pp1_energy(RAPL rapl);
double dram_energy(RAPL rapl);

int detect_pp1();

void closef(RAPL **rapl);


#endif