#include "cpuid.h"


CPUID* cpuid(uint32_t leaf, uint32_t subleaf)
{
    struct CPUID *cpuid = malloc(sizeof(struct CPUID));
   
    asm volatile
            ("cpuid" : "=a" (cpuid->eax), "=b" (cpuid->ebx), "=c" (cpuid->ecx), "=d" (cpuid->edx)
             : "a" (leaf), "c" (subleaf));
        // ECX is set to zero for CPUID function 4
    
    return cpuid;
}