

#ifndef WRMSR_H
#define WRMSR_H

#include <dirent.h>
   

int dir_filter_w(const struct dirent *dirp);

void wrmsr_on_cpu(uint32_t reg, int cpu, int valcnt, char *regvals);
void wrmsr_on_all_cpus(uint32_t reg, int valcnt, char *regvals);


#endif