
#include "topology.h"

#include "cpuid.h"

GLKTSN_T glbl_ptr = {0,0,0,0,0};






/* ------------------- */

// Install the affinity mask for the process.  Some Linux distros have 
// the length of the mask as the second argument of sched_setaffinity().
//
int misc_sched_setaffinity( pid_t pid, cpu_set_t * affinity_mask )
{
    int stat = -1;

#if __GNUC__== 3 && __GNUC_MINOR__== 2 
    stat = sched_setaffinity( pid, affinity_mask );

#else
    stat = sched_setaffinity( pid, sizeof(*affinity_mask),
				    affinity_mask );
#endif

    return stat;
}

// Get the affinity mask for this process.  Some Linux distros have
// the length of the mask as the second argument, and others do not.
// Check sched.h on your system.
//
int misc_sched_getaffinity( pid_t pid, cpu_set_t * affinity_mask )
{
    int stat = -1;

#if __GNUC__== 3 && __GNUC_MINOR__== 2 
    stat = sched_getaffinity( pid, affinity_mask );

#else
    stat = sched_getaffinity( pid, sizeof(*affinity_mask), 
				     affinity_mask );
#endif

    return stat;
}

int CPUTopologyParams()
{
   
    CPUID *cpuID0 = NULL, *cpuID1 = NULL, *cpuID0B = NULL;// = malloc(sizeof(struct CPUID));
    
    cpuID0 = cpuid(0, 0);
     
    int maxCPUID = (cpuID0)->eax; // highest CPUID leaf index this processor supports
    int hasLeafB = 0;
    free(cpuID0);
    
    if( maxCPUID > 0x0B )
    {
        // CPUID *cpuID0B = malloc(sizeof(struct CPUID));
        cpuID0B = cpuid(0x0B, 0);
        hasLeafB = ((cpuID0B)->ebx != 0);
        free(cpuID0B);
    }
     
    // Use HWMT feature flag CPUID.01:EDX[28] to treat three configurations:
    cpuID1 = cpuid(1, 0);
    unsigned int HWMT = ((cpuID1)->edx & HWMT_BIT) >> 28;
    free(cpuID1);
    
    if(HWMT) {
        // #1, Processors that support leaf 0BH
        if(hasLeafB) {
            // use CPUID leaf B to derive extraction parameters
            CPUTopologyLeafBConstants();
            //printf("Support Leaf 0x0B\n");
        } 
        // #2: Processors that support legacy parameters 
        //      using CPUID leaf 1 and leaf 4
        else {
            
        }
    } else {

    }

    // *cpuID = cpuid(0x04, 3);
    // printf("Type %d\n", ((*cpuID)->eax) & 0x1f);
    // unsigned level = (((*cpuID)->eax) >> 5) & 0x7;
    // printf("level: %d\n", level);

   
    return 0;
}


int CPUTopologyLeafBConstants()
{
    CPUID *cpuID0B = NULL;
    int levelType, levelShift;
    int subLeaf = 0;

    do {
        // we already tested CPUID leaf 0BH contain valid sub-leaves,
        cpuID0B = cpuid(0x0B, subLeaf);
        if(cpuID0B->ebx == 0) {
            free(cpuID0B);
            break;    
        }
        levelType = (cpuID0B->ecx >> 8) & 0xFF;
        levelShift = (cpuID0B->eax& 0x1F);
        free(cpuID0B);
        switch (levelType)
        {
        case 1: // level type is SMT, so levelShift is the SMT_Mask_Width
            glbl_ptr.SMTSelectMask = ~((-1) << levelShift);
            glbl_ptr.SMTMaskWidth = levelShift;
            //SMT_MASK = ~((-1) << levelShift);
            //Mask_SMT_shift = levelShift;
            //cout << levelType << ":" << Mask_SMT_shift << endl;
            break;
        case 2:
            glbl_ptr.CoreSelectMask = pow(~((-1) << levelShift), glbl_ptr.SMTSelectMask);
            glbl_ptr.PkgSelectMask = (-1) << levelShift;
            //Mask_Core_shift = levelShift;
            //COREPlusSMT_MASK = ~((-1) << Mask_Core_shift);
            //CORE_MASK = pow(COREPlusSMT_MASK, SMT_MASK);
            //PACKAGE_MASK = (-1) << Mask_Core_shift;
            //cout << Mask_Core_shift << ":" << COREPlusSMT_MASK << ":" << CORE_MASK << ":" << PACKAGE_MASK << endl;
            break;
        default:
            break;
        }
        subLeaf++;
    } while (1);

    
  
    return 0;
}


// Query the x2APIC ID of a logical processor.
int getx2APIC_ID() 
{
    CPUID *cpuID0B;
    cpuID0B = cpuid(0x0B, 0);
    uint32_t val = cpuID0B->edx;
    free(cpuID0B);
    return val;
}


int threeLevelExtractionAlgorithm(Topology *topology)
{
     int apicID;
    //int tabPkgID[MAX_CPUS], tabCoreID[MAX_CPUS], tabSMTID[MAX_CPUS];


    // Determine the number of processors are available to run this process. 
    //
    int numProcessors = sysconf(_SC_NPROCESSORS_CONF); 
    //cout << "numProcessors: " << numProcessors<< endl;

    // Get the system affinity mask.
    //
    cpu_set_t sysAffinityMask;	 
    //misc_sched_getaffinity(0, &sysAffinityMask);


    // Number of logical processors on each processor package.
    //
    CPUID *cpuID01;
    cpuID01 = cpuid(1, 0);

    topology->max_logicalPerPack = ((*cpuID01).ebx & NUM_LOGICAL_BITS) >> 16;
    free(cpuID01);
    //printf("logicalPerPack: %d\n", topology->max_logicalPerPack );
    //exit(0);
    /// Number of cores per processor package.
    //
    CPUID *cpuID04;
    cpuID04 = cpuid(0x04, 0);
    topology->max_corePerPack = ((cpuID04->eax & CORES_PER_PROCPAK) >> 26) + 1;
    free(cpuID04);
    //corePerPack = (( cpuID04.EAX() & CORES_PER_PROCPAK ) >> 26 ) + 1; //multiCoresPerProcPak();


    // Number of logical processors on each core.  Assume that cores 
    // within a package have the same number of logical processors. 
    //
    topology->logicalPerCore = topology->max_logicalPerPack  / topology->max_corePerPack;

    // Find the affinity and IDs of each logical processor in the
    // system by reading the initial APIC for each processor. 
    // 
    unsigned int affinityMask = 1;
    cpu_set_t currentCPU;

    //unsigned int packageIDMask;
    int i = 0;

    while ( i < numProcessors )
    {
        CPU_ZERO(&currentCPU);
        CPU_SET(i, &currentCPU);
        if( misc_sched_setaffinity(getpid(), &currentCPU) == 0 )
        {
            // Ensure system has switched to the right CPU
            sleep(0);

            // Get the initial APIC ID for this processor
            apicID = getx2APIC_ID();

            //procData[i][0] = affinityMask;
            //procData[i][1] = apicID;
            topology->tbAffinityMask[i] = affinityMask;
            topology->tbInitialAPIC[i] = apicID;

            //procData[i][4] = apicID & SMT_MASK;
            //procData[i][3] = (apicID & CORE_MASK) >> SMT_MASK;
            //procData[i][2] = (apicID & PACKAGE_MASK) >> CORE_MASK;
            
            topology->tbPkgID[i] = (apicID & glbl_ptr.PkgSelectMask) >> glbl_ptr.CoreSelectMask;
            topology->tbCoreID[i] = (apicID & glbl_ptr.CoreSelectMask) >> glbl_ptr.SMTSelectMask;
            topology->tbSMTID[i] = apicID & glbl_ptr.SMTSelectMask;
            //cout << "AffinityMask = 0x" << hex << procData[i][0] << "\t" << "Initial APIC = 0x" << hex << procData[i][1] << "\t" << "Physical ID = " << procData[i][2] << "\t" <<  "Core ID = " << procData[i][3] << "\t" << "SMT ID = " << procData[i][4] << endl;
            
            // Number of available logical processors in the system.
	        //
	        topology->numSMT++;//availLogical ++;   

        }

        i++;
        affinityMask = (1 << i);
    }

    // restore the affinity setting to its original state
    //
    misc_sched_setaffinity (0, &sysAffinityMask);


    // Count the available cores in the system
    //
    topology->numCores = countAvailableCores(topology->tbPkgID, topology->tbCoreID, topology->numSMT);  
   
    //cout << "availCore: " << availCore << endl;
    // Count the physical processors in the system
    //
    topology->numPackages = countPhysicalPacks( topology->tbPkgID, topology->numSMT);
    


    //
    // Check to see if the system is multi-core or
    // contains hyper-threading technology.
    //
    if ( topology->numCores > topology->numPackages ) 
    {
	// Multi-core
	if (topology->logicalPerCore == 1)
	    return MULTI_CORE_AND_HT_NOT_CAPABLE;

	else if (topology->numSMT > topology->numCores)
	    return MULTI_CORE_AND_HT_ENABLED;
	else 
	    return MULTI_CORE_AND_HT_DISABLED;
	
    }
    else
    {
	// Single-core
	if (topology->logicalPerCore == 1)
	    return SINGLE_CORE_AND_HT_NOT_CAPABLE;

	else if (topology->numSMT > topology->numCores)
	    return SINGLE_CORE_AND_HT_ENABLED;
	else 
	    return SINGLE_CORE_AND_HT_DISABLED;
    }
    

    return USER_CONFIG_ISSUE;
}



//  Using the list of CORE_ID to count the number of cores in a MP system and construct, for each core, a multi-bit mask corresponding
//  to those logical processors residing in the same core.
//
//  Processors in the same core can be determined by bucketing the processors with the same PACKAGE_ID and CORE_ID. Note that code
//  below can BIT OR the values of PACKGE and CORE ID because they have not been shifted right.
//
//  The algorithm below assumes there is symmetry across package boundary if more than one socket is populated in an MP system.

//  int topology::countAvailableCores(int tblPkgID[], int tabCoreID[], int numLPEnabled)
int countAvailableCores(int tbPkgID[], int tbCoreID[], int numLPEnabled)
{
    int i, processorNum;
    int coreNum = 1;
    int coreIDBucket[numLPEnabled], coreProcessorMask[numLPEnabled];
    int processorMask = 1;

    coreIDBucket[0] = tbPkgID[0] | tbCoreID[0];
    coreProcessorMask[0] = processorMask;
    
    for( processorNum = 1; processorNum < numLPEnabled; processorNum++)
    {
        processorMask <<= 1;
        for(i=0; i < coreNum; i++)
        {
            // we may be comparing bit-fields of logical processors residing in different
            // packages, the code below assume package symmetry
            if( (tbPkgID[processorNum] | tbCoreID[processorNum]) == coreIDBucket[i])
            {
                coreProcessorMask[i] |= processorMask;
                break; // found in existing bucket, skip to next iteration
            }
        }

        if( i == coreNum )
        {
            //Did not match any bucket, start new bucket
            coreIDBucket[i] = tbPkgID[processorNum] | tbCoreID[processorNum];
            coreProcessorMask[i] = processorMask;
            coreNum++;
        }
    }

    // CoreNum has the number of cores started in the OS
    // CoreProcessorMask[] array has the processor set of each core
    return coreNum;
}

// Count the physical processors in the system
// Using the list of PACKAGE_ID to count the number of physical packages in a MP system and construct, for each package, a multi-bit
// mask corresponding to those logical processors residing in the same package.
//      Compute the number of packages by counting the number of processors
//      with unique PACKAGE_IDs in the PackageID array.
//      Compute the mask of processors in each package.
int countPhysicalPacks(int tbPkgID[], int numLPEnabled)
{
    // PackageIDBucket is an array of unique PACKAGE_ID values. Allocate an array of
    // NumStartedLPs count of entries in this array.
    // PackageProcessorMask is a corresponding array of the bit mask of processors belonging to
    // the same package, these are processors with the same PACKAGE_ID
    // The algorithm below assumes there is symmetry across package boundary if more than
    // one socket is populated in an MP system.
    // Bucket Package IDs and compute processor mask for every package.

    int packageNum = 1;
    int packageIDBucket[numLPEnabled], packageProcessorMask[numLPEnabled];
    int processorMask = 1;
    int i, processorNum;

    packageIDBucket[0] = tbPkgID[0];
    packageProcessorMask[0] = processorMask;


    for(processorNum = 1; processorNum < numLPEnabled; processorNum++)
    {

        processorMask <<= 1;
        for( i=0; i < packageNum; i++)
        {
            // we may be comparing bit-fields of logical processors residing in different
            // packages, the code below assume package symmetry

            if( tbPkgID[processorNum] == packageIDBucket[i])
            {
                packageProcessorMask[i] |= processorMask;
                break; // found in existing bucket, skip to next iteration
            }
        }

        if( i == processorNum )
        {
            //PACKAGE_ID did not match any bucket, start new bucket
            packageIDBucket[i] = tbPkgID[processorNum];
            packageProcessorMask[i] = processorMask;
            packageNum++;
        }
    }

    // PackageNum has the number of Packages started in OS
    // PackageProcessorMask[] array has the processor set of each package
    return packageNum;
}

// Print the attributes of the processor.
//
void printAttributes(int sysAttributes, int numPackages)
{ 
    printf("\nCapabilities:\n\n");
    
    switch( sysAttributes )
    {	
    case MULTI_CORE_AND_HT_NOT_CAPABLE:
	printf("\tHyper-Threading Technology: Not capable  \n\tMulti-core: Yes \n\tMulti-processor: ");
	break;
	
    case SINGLE_CORE_AND_HT_NOT_CAPABLE:
	printf("\tHyper-Threading Technology: Not capable  \n\tMulti-core: No \n\tMulti-processor: ");
	break;
	
    case SINGLE_CORE_AND_HT_DISABLED:
	printf("\tHyper-Threading Technology: Disabled  \n\tMulti-core: No \n\tMulti-processor: ");
	
    case SINGLE_CORE_AND_HT_ENABLED:
	printf("\tHyper-Threading Technology: Enabled  \n\tMulti-core: No \n\tMulti-processor: ");
	break;
	
    case MULTI_CORE_AND_HT_DISABLED:
	printf("\tHyper-Threading Technology: Disabled  \n\tMulti-core: Yes \n\tMulti-processor: ");
	break;
	
    case MULTI_CORE_AND_HT_ENABLED:
	printf("\tHyper-Threading Technology: Enabled  \n\tMulti-core: Yes \n\tMulti-processor: ");
	break;
	
    case USER_CONFIG_ISSUE:
	printf("User Configuration Error: Not all logical processors in the system are enabled \
	while running this process. Please rerun this application after making corrections. \n");
	exit(1);
	break;
	
    default:
	printf("Error: Unexpected return value.\n");
	exit(1);
	
    }
    if (numPackages > 1) printf("Yes\n"); else printf("No\n");
    
}

// Print the system's capabilities.
//
void printCapabilities( int numPackages, int numCores, int numSMT,
			int corePerPack, int logicalPerCore )
{
    printf("\n\nHardware capability and its availability to applications: \n");
    printf("\n  System wide availability: %d physical processors, %d cores, %d logical processors\n", \
	   numPackages, numCores, numSMT);
    
    printf("  Multi-core capabililty : %d cores per package \n", corePerPack );
    printf("  HT capability: %d logical processors per core \n", logicalPerCore );
    
    if ( numPackages * corePerPack > numCores) 
	printf("\n  Not all cores in the system are enabled for this application.\n");
    else 
	printf("\n  All cores in the system are enabled for this application.\n");
}

// Print out the affinity of each processor and its IDs.
//
void printAffinity(Topology topology)
{
    printf("\n\nRelationships between OS affinity mask, Initial APIC ID, and 3-level sub-IDs: \n\n\n");
    for(int i = 0; i < topology.numSMT; i++)
    {
        printf("AffinityMask = 0x%x\tInitial APIC = 0x%d\tPackage ID = %d; Core ID = %d; SMT ID = %d\n", topology.tbAffinityMask[i], topology.tbInitialAPIC[i], topology.tbPkgID[i], topology.tbCoreID[i], topology.tbSMTID[i]);
        //printf("AffinityMask = 0x%d\tInitial APIC = 0x%d\tPhysical ID = " << tabPkgID[i] << "      " <<  "Core ID = " << tabCoreID[i] << "      " << "SMT ID = " << tabSMTID[i]);
    }
    //topology.numCores = 55;
}