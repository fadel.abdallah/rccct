#include<stdio.h>
#include <inttypes.h>
#include<math.h>
#include <string.h>

#include "event.h"
#include "cpuinfo.h"
#include "rapl.h"

#define MAX_NB_EVENTS 500

typedef struct Sample_s
{
    /* data */
    RAPL **rapl;
    uint64_t **counters;
} Sample;

typedef struct Element Element;
struct Element
{
    Event event;
    struct Element* next;
};

typedef Element* llist;


llist addAtEnd(llist list, Event e)
{
    Element* element = malloc(sizeof(Element));
    element->event = e;
    element->next = NULL;

    if (list == NULL)
    {
        return element;
    }
    else
    {
        Element* temp = list;
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = element;   
        return list;
    }
}


void printList(llist list)
{
    Element* tmp = list;

    while (tmp != NULL)
    {
        printf("%s\n", tmp->event.name);
        tmp = tmp->next;
    }
    
}
/*
void sortEventsDescWithCorPower(int numEvents, Event events[numEvents], int nbSamples, int generalCounters, int numSMT)
{

}
*/



int hasActiveEvents(int numEvents, Event events[numEvents])
{
    for (int i = 0; i < numEvents; i++)
    {
        if(events[i].active == 1)
            return 1;
    }
    return 0;
}

double calculateCorrelationCofficient(Sample *sample1, Sample *sample2, int numSamples, int numSMT)
{
    double sum_X = 0; // Coounter Event 1
    double sum_Y = 0; //  Coounter Event 2
    double sum_XY = 0; //  Coounter Event 1 *  Coounter Event 2
    double squareSum_X = 0; 
    double squareSum_Y = 0;
    
    double counters[2][numSamples];
    //double totalSum[numEvents];
    uint64_t minCounters[2];
    uint64_t maxCounters[2];
    uint64_t maxSample1 = 0, maxSample2 = 0, minSample1 = 0, minSample2 = 0;
    uint64_t sum_CountersSample1 = 0;
    uint64_t sum_CountersSample2 = 0;


    for (int cpu = 0; cpu < numSMT; cpu++)
    {
        minSample1 = minSample1 + sample1->counters[0][cpu];
        minSample2 = minSample2 + sample2->counters[0][cpu];
    }
    maxSample1 = minSample1;
    maxSample2 = minSample2;

    for (size_t s = 1; s < numSamples; s++)
    {
        sum_CountersSample1 = 0;
        sum_CountersSample2 = 0;
        for (int cpu = 0; cpu < numSMT; cpu++)
        {
            // sum of counters of event e (one counter by logical processor)
            sum_CountersSample1 += sample1->counters[s][cpu];
            sum_CountersSample2 += sample2->counters[s][cpu];
            //printf("%d  %d c %ld\n",e, s, sum_Counters);
        }
        
        if (sum_CountersSample1 > maxSample1)
        {
            maxSample1 = sum_CountersSample1;
        }
        if (sum_CountersSample1 < minSample1)
        {
            minSample1 = sum_CountersSample1;
        }

        if (sum_CountersSample2 > maxSample2)
        {
            maxSample2 = sum_CountersSample2;
        }
        if (sum_CountersSample2 < minSample2)
        {
            minSample2 = sum_CountersSample2;
        }
            //totalSum[e] += sum_Counters;
    
        
    }

    minCounters[0] = minSample1;
        minCounters[1] = minSample2;
        maxCounters[0] = maxSample1;
        maxCounters[1] = maxSample2;
    //printf("min %ld\tmax %ld\n", minCounters[0], maxCounters[0]);
    //printf("min %ld\tmax %ld\n", minCounters[1], maxCounters[1]);
    for(size_t s = 0; s < numSamples; s++)
    {
        sum_CountersSample1 = sum_CountersSample2 = 0;
        for (int cpu = 0; cpu < numSMT; cpu++)
        {
            // sum of counters of event e (one counter by logical processor)
            sum_CountersSample1 += sample1->counters[s][cpu];
            sum_CountersSample2 += sample2->counters[s][cpu];
            //printf("%d  %d c %ld\n",e, s, sum_Counters);
        }

        counters[0][s] = ((sum_CountersSample1 - minCounters[0]) * 1.0/(maxCounters[0] - minCounters[0]));
        counters[1][s] = ((sum_CountersSample2 - minCounters[1]) * 1.0/(maxCounters[1] - minCounters[1]));
        //printf("sum %ld\tc1 %f\n",sum_CountersSample1 , counters[0][s]);
    }
    //printf("%f\t%f\t%f\t%f\t%f\n", sum_X, sum_Y, sum_XY, squareSum_X, squareSum_Y);
    //sum_X[e] = sum_Y[e] = sum_XY[e] = squareSum_X[e] = squareSum_Y[e] = 0;
    for (int s = 0; s < numSamples; s++)
    {
        sum_X += counters[0][s];
        sum_Y += counters[1][s];
        // sum of counters * consumption_power 
        sum_XY += counters[0][s] * counters[1][s];
        //printf("c %ld * p %f  = sum_XY %f \n", sum_Counters, power, sum_XY[e]);
        //printf("c %ld * p %.10f  = sum_XY %f \n", sum_Counters, power, sum_Counters * power);
        // sum of square of array elements
        squareSum_X = squareSum_X + counters[0][s] * counters[0][s];
        squareSum_Y = squareSum_Y + counters[1][s] * counters[1][s];

             
        //printf("%f\t%f\t%f\t%f\t%f\n", sum_X, sum_Y, sum_XY, squareSum_X, squareSum_Y);
    }

    // Return correlation coeffficient 

    return (double)(numSamples * sum_XY- sum_X * sum_Y)
                    / sqrt((numSamples * squareSum_X - sum_X * sum_X) * 
                    (numSamples * squareSum_Y - sum_Y * sum_Y)); 
}

void RCCT(int numSamples, int numEvents, Event events[numEvents],  int numSMT, int numGeneralCounters, double threshold)
{
    llist list = NULL;

    Event e;
    Sample *sample = NULL;
    uint64_t perfCounterEvents_before[numGeneralCounters][numSMT];
    uint64_t perfCounterEvents_after[numGeneralCounters][numSMT];
    int address = 390;
    uint32_t performanceCounter = 193;
    char str1[10] = "";
    int c = 0, k = 0;

    sample = malloc(numGeneralCounters * sizeof(Sample));
    for (int e = 0; e < numGeneralCounters; e++)
    {
        (sample + e)->rapl = calloc(numSamples, sizeof(RAPL*));
        (sample + e)->counters = calloc(numSamples, sizeof(uint64_t*));
        for (int s = 0; s < numSamples; s++)
        {
            (sample + e)->counters[s] = calloc(numSMT, sizeof(uint64_t));
        }
    }
    

    while (hasActiveEvents(numEvents, events))
    {


        for (c = 0; c < numGeneralCounters; c++)
        {
            /* code */
            if(events[c].active == 1)
            {
                str1[0] = '\0';
                strcat(str1, "0x0043");
                strcat(str1, events[c].umask + 2);
                strcat(str1, events[c].code + 2);
                printf("%s\n", str1);
                wrmsr_on_all_cpus(address + c, 1, str1);
            }
        }

        for (int s = 0; s < numSamples; s++)
        {
            for (int i = 0; i < c; i++)
            {
                //int index = (indexEvent * numGeneralCounters) + c;
                rdmsr_on_all_cpus_events(performanceCounter + i, numSMT, perfCounterEvents_before, i);
            }
            (sample + 0)->rapl[s] = rapl_init(0);
           
           
            // pause x ms
            usleep(1000);


            sample_rapl(&(sample + 0)->rapl[s]);
            closef(&(sample + 0)->rapl[s]);
            //printf("pw %f \n", pp0_current_power(*((sample + s)->rapl[indexEvent])));

            for (int i = 0; i < c; i++)
            {
                //int index = (indexEvent * numGeneralCounters) + c;
                rdmsr_on_all_cpus_events(performanceCounter + i, numSMT, perfCounterEvents_after, i);

                //printf("c %ld\n", perfCounterEvents_after[c][0] - perfCounterEvents_before[c][0]);
            }

            for (int i = 0; i < c; i++)
            {
                //int index = (indexEvent * numGeneralCounters) + c;
                //printf("Index %d\n", index);
                for(int cpu = 0; cpu < numSMT; cpu++)
                {
                    (sample + i)->counters[s][cpu] = perfCounterEvents_after[i][cpu] - perfCounterEvents_before[i][cpu];
                    //printf("%ld\t", (sample + i)->counters[s][cpu]);
                   //printf("x %ld sample %ld c %ld\n", perfCounterEvents_after[c][cpu], perfCounterEvents_before[c][cpu], ((sample + index)->counters[s][cpu] ));
                }
                //printf("\n");
                
            }
            //printf("------------------------------\n");
            //(sample + index)->rapl[s] = rapl_init(0);
        }

        for (int i = 0; i < c; i++)
        {
            for (int j = i + 1; j < c; j++)
            {
                if(events[j].active == 1)
                {
                    double corr = calculateCorrelationCofficient((sample + i),(sample + j), numSamples, numSMT);
                    //printf("%s:::%s\tcorr %f\n", events[i].name, events[j].name, corr);
                    if (corr >= threshold)
                    {
                        events[j].active = 0;
                    }
                }
            }
            
        }


        for (int i = c; i < numEvents;)
        {
            for (k = 0; k < (numGeneralCounters - 1) && ((k + i) < numEvents); k++)
            {
                if(events[i + k].active == 1)
                {
                    str1[0] = '\0';
                    strcat(str1, "0x0043");
                    strcat(str1, events[k + i].umask + 2);
                    strcat(str1, events[k + i].code + 2);
                    //printf("i %d k %d\t %s\t%s\n",i, k, events[k + i].name, str1);
                    wrmsr_on_all_cpus(address + k, 1, str1);
                }
            }
            for (int s0 = 0; s0 < c; s0++)
            {
                if (events[s0].active == 1)
                {
                    str1[0] = '\0';
                    strcat(str1, "0x0043");
                    strcat(str1, events[s0].umask + 2);
                    strcat(str1, events[s0].code + 2);
                    //printf("%s\n", str1);
                    wrmsr_on_all_cpus(address + k, 1, str1);
                


                    for (int s = 0; s < numSamples; s++)
                    {
                        for (int p = 0; p < (k+1); p++)
                        {
                            rdmsr_on_all_cpus_events(performanceCounter + p, numSMT, perfCounterEvents_before, p);
                        }

                        //(sample + 0)->rapl[s] = rapl_init(0);
            
            
                        // pause x ms
                        usleep(1000);


                        //sample_rapl(&(sample + 0)->rapl[s]);
                        //closef(&(sample + 0)->rapl[s]);
                        //printf("pw %f \n", pp0_current_power(*((sample + s)->rapl[indexEvent])));

                    for(int p = 0; p < (k+1); p++)
                    {
                            //int index = (indexEvent * numGeneralCounters) + c;
                            rdmsr_on_all_cpus_events(performanceCounter + p, numSMT, perfCounterEvents_after, p);

                            //printf("c %ld\n", perfCounterEvents_after[c][0] - perfCounterEvents_before[c][0]);
                    }

                    for(int p = 0; p < (k+1); p++)
                    {
                        for(int cpu = 0; cpu < numSMT; cpu++)
                        {
                            (sample + p)->counters[p][cpu] = perfCounterEvents_after[p][cpu] - perfCounterEvents_before[p][cpu];
                                //printf("%ld ", (sample + p)->counters[p][cpu]  );
                        }
                        //printf("\n");
                    }
                        
                    }

                    for (int e = 0; e < k; e++)
                    {
                        double corr = calculateCorrelationCofficient((sample + e),(sample + k), numSamples, numSMT);
                        //printf("%s:::%s\tcorr %f\n", events[e + i].name, events[s0].name, corr);
                        if (corr >= threshold)
                        {
                            events[e + i].active = 0;
                        }
                    }
                    
                }
                //printf("i %d k %d\n", i, k);
                

            }
            i = i + k;
        }

        for (int i = 0; i < numGeneralCounters; i++)
        {
            if(events[i].active == 1)
            {
                list = addAtEnd(list, events[i]);
                events[i].active = 0;
            }
        }

        for (int i = 0; i < numEvents; i++)
        {
            if (events[i].active == 0)
            {
                for (int j = i + 1 ; j < numEvents; j++)
                {
                    if(events[j].active == 1)
                    {
                        e = events[i];
                        events[i] = events[j];
                        events[j] = e;
                        break;
                    }
                }
            }
        }
        

        printf("-------------------------------------");
        for (int i = 0; i < numEvents; i++)
        {
            printf("%s\t%d\n", events[i].name, events[i].active);
        }

        printf("----------------------------------\n");
        printList(list);
        
        
        
        //break;
    }
}



Sample* samplesUsedForSortEvents(int numEvents, Event events[numEvents], int numSamples, int numGeneralCounters, int numSMT)
{
    Sample *sample = NULL;
    uint64_t perfCounterEvents_before[numGeneralCounters][numSMT];
    uint64_t perfCounterEvents_after[numGeneralCounters][numSMT];
    int address = 390;
    uint32_t performanceCounter = 193;
    char str1[10] = "";
    int indexEvent = 0, c;

    sample = malloc(numEvents * sizeof(Sample));

    int numIterations = numEvents/numGeneralCounters + ((numEvents % numGeneralCounters) == 0 ? 0 : 1);
    //printf("Iters %d\n", numIterations);

    for (int e = 0; e < numEvents; e++)
    {
        (sample + e)->rapl = calloc( numSamples, sizeof(RAPL*));
        (sample + e)->counters = calloc( numSamples, sizeof(uint64_t*));
        for (int s = 0; s < numSamples; s++)
        {
            (sample + e)->counters[s] = calloc(numSMT, sizeof(uint64_t));
        }
    }


    while (indexEvent < numIterations)
    {
        for (c = 0; c < numGeneralCounters && (c + indexEvent * numGeneralCounters) < numEvents; c++)
        {
            int index = (indexEvent * numGeneralCounters) + c;
            str1[0] = '\0';
            strcat(str1, "0x0043");
            strcat(str1, events[index].umask + 2);
            strcat(str1, events[index].code + 2);
            printf("%s\n", str1);
            wrmsr_on_all_cpus(address + c, 1, str1);
        }

        for (int s = 0; s < numSamples; s++)
        {
            for (c = 0; c < numGeneralCounters && (c + indexEvent * numGeneralCounters) < numEvents; c++)
            {
                //int index = (indexEvent * numGeneralCounters) + c;
                rdmsr_on_all_cpus_events(performanceCounter + c, numSMT, perfCounterEvents_before, c);
            }
            (sample + indexEvent)->rapl[s] = rapl_init(0);
           
           
            // pause x ms
            usleep(1000);


            sample_rapl(&(sample + indexEvent)->rapl[s]);
            closef(&(sample + indexEvent)->rapl[s]);
            //printf("pw %f \n", pp0_current_power(*((sample + s)->rapl[indexEvent])));

            for (c = 0; c < numGeneralCounters && (c + indexEvent * numGeneralCounters) < numEvents; c++)
            {
                //int index = (indexEvent * numGeneralCounters) + c;
                rdmsr_on_all_cpus_events(performanceCounter + c, numSMT, perfCounterEvents_after, c);

                //printf("c %ld\n", perfCounterEvents_after[c][0] - perfCounterEvents_before[c][0]);
            }

            for (c = 0; c < numGeneralCounters && (c + indexEvent * numGeneralCounters) < numEvents; c++)
            {   
                int index = (indexEvent * numGeneralCounters) + c;

                for(int cpu = 0; cpu < numSMT; cpu++)
                {
                    (sample + index)->counters[s][cpu] = perfCounterEvents_after[c][cpu] - perfCounterEvents_before[c][cpu];
                    //printf("%ld\t", (sample + index)->counters[s][cpu]);
                    //printf("x %ld sample %ld c %ld\n", perfCounterEvents_after[c][cpu], perfCounterEvents_before[c][cpu], ((sample + index)->counters[s][cpu] ));
                }
                //printf("\n");
                
            }
            //printf("------------------------------\n");
            //(sample + index)->rapl[s] = rapl_init(0);
        }
        
        

        indexEvent++;
       
    }
   
    ///printf("fin function SamplesUsedForSortEvents\n");
    return sample;
}



void sortEventsDescWithCorrPower(Sample** samples, int numSamples, int numEvents, Event events[numEvents], int numSMT, int numGeneralCounters)
{
    Event e;
    double* sum_X = calloc(numEvents, sizeof(double)); // Coounters
    double* sum_Y = calloc(numEvents, sizeof(double));     // Power consumption
    double* sum_XY = calloc(numEvents, sizeof(long double));  // Coounters * Power consumption
    double* squareSum_X = calloc(numEvents, sizeof(double)); 
    double* squareSum_Y = calloc(numEvents, sizeof(double));
    //double* corr_XY = calloc(numEvents, sizeof(double));
    
    
    double counters[numEvents][numSamples];
    double totalSum[numEvents];
    uint64_t minCounters[numEvents];
    uint64_t maxCounters[numEvents];
    uint64_t max = 0, min = 0;
    
    for (int e = 0; e < numEvents; e++)
    {
       
        for (int cpu = 0; cpu < numSMT; cpu++)
        {
                // sum of counters of event e (one counter by logical processor)
                 min += (*samples + e)->counters[0][cpu];
                 //printf("%d  %d c %ld\n",e, s, sum_Counters);
        }
        //totalSum[e] = max = min;
        
        for (size_t s = 1; s < numSamples; s++)
        {
            uint64_t sum_Counters = 0;
            for (int cpu = 0; cpu < numSMT; cpu++)
            {
                // sum of counters of event e (one counter by logical processor)
                 sum_Counters += (*samples + e)->counters[s][cpu];
                 //printf("%d  %d c %ld\n",e, s, sum_Counters);
            }
            if (sum_Counters > max)
            {
                max = sum_Counters;
            }
            if (sum_Counters < min)
            {
                min = sum_Counters;
            }
            //totalSum[e] += sum_Counters;
        }
        minCounters[e] = min;
        maxCounters[e] = max;
    }
    
    for (int e = 0; e < numEvents; e++)
    {
        for (size_t s = 0; s < numSamples; s++)
        {
            uint64_t sum_Counters = 0;
            for (int cpu = 0; cpu < numSMT; cpu++)
            {
                // sum of counters of event e (one counter by logical processor)
                 sum_Counters += (*samples + e)->counters[s][cpu];
                 //printf("%d  %d c %ld\n",e, s, sum_Counters);
            }

            counters[e][s] = ((sum_Counters - minCounters[e]) * 1.0/(maxCounters[e] - minCounters[e]));
            totalSum[e] += counters[e][s];
        }
        
    }


    for (int e = 0; e < numEvents; e++)
    {
        for (size_t s = 0; s < numSamples; s++)
        {
            counters[e][s] = counters[e][s] - totalSum[e] * 1.0/numSamples ;
        }
        
    }

    // correlation coeffficient 
    for (int e = 0; e < numEvents; e++)
    {
       //int numIterations = e/numGeneralCounters;
        //sum_X[e] = sum_Y[e] = sum_XY[e] = squareSum_X[e] = squareSum_Y[e] = 0;
        for (int s = 0; s < numSamples; s++)
        {
           // double sum_Counters = 0;
            //for (int cpu = 0; cpu < numSMT; cpu++)
            //{
                // sum of counters of event e (one counter by logical processor)
                // sum_Counters += counters[e][s];
                 //printf("%d  %d c %ld\n",e, s, sum_Counters);
            //}
            //printf("\n");
            // sum of counters for all samples
            sum_X[e] += counters[e][s];
            // sum of consumption power for all samples
            double power = pp0_current_power(*((*samples + (e/numGeneralCounters))->rapl[s])); //roundf(pp0_current_power(*((*samples + (e/numGeneralCounters))->rapl[s])) * 100) *1.0/100;
            //printf("power %lf\n", power);
            sum_Y[e] += power;
            // sum of counters * consumption_power 
            sum_XY[e] += (counters[e][s] * power);
            //printf("c %ld * p %f  = sum_XY %f \n", sum_Counters, power, sum_XY[e]);
            //printf("c %ld * p %.10f  = sum_XY %f \n", sum_Counters, power, sum_Counters * power);
            // sum of square of array elements
            squareSum_X[e] = squareSum_X[e] + counters[e][s] * counters[e][s];
            squareSum_Y[e] = squareSum_Y[e] + power * power;

             
            //printf("%f\t%f\t%f\t%f\t%f\n", sum_X[e], sum_Y[e], sum_XY[e], squareSum_X[e], squareSum_Y[e]);
        }
       //printf("%s\t%ld\t%f\t %ld\n", events[e].name, squareSum_X[e], sum_XY[e], sum_X[e] * sum_X[e]);
        // use formula for calculating correlation coefficient 
        events[e].correlationWithPower = (double)(numSamples * sum_XY[e]- sum_X[e] * sum_Y[e])
                    / sqrt((numSamples * squareSum_X[e] - sum_X[e] * sum_X[e]) * 
                    (numSamples * squareSum_Y[e] - sum_Y[e] * sum_Y[e])); 
    }

    for (int i = 0; i < numEvents; i++)
    {
        for (int j = i + 1; j < numEvents; j++)
        {
            if (events[i].correlationWithPower < events[j].correlationWithPower)
            {
                e = events[i];
                events[i] = events[j];
                events[j] = e;
            }
            
        }
            
    }

    // Print the events in descending order according to their correlations with the power
    for (int i = 0; i < numEvents; i++)
    {
         printf("%s %f\n",events[i].name, events[i].correlationWithPower);
    }
    
    
    
}

int main(int argc, char const *argv[])
{
    Sample *sample;
    CPUInfo *CPUInfo;
    Event events[MAX_NB_EVENTS];
    int numEvents;
    double threshold = 0.8;

    CPUInfo = cpuinfo();
    CPUTopologyParams();
    CPUInfo->topology = calloc(1, sizeof(struct Topology));
    int sysAttributes = threeLevelExtractionAlgorithm(CPUInfo->topology);


    //printAttributes(sysAttributes,CPUInfo->topology->numPackages);
    //printCapabilities(CPUInfo->topology->numPackages, CPUInfo->topology->numCores, CPUInfo->topology->numSMT,
	//		CPUInfo->topology->max_corePerPack, CPUInfo->topology->logicalPerCore);
    //printAffinity(*(CPUInfo->topology));
    
    //printf("%d\n", CPUInfo->topology->numSMT);
    //printf("%d\n", CPUInfo->topology->numCores);
    printf("Number of general counters: %d\n", CPUInfo->perfGeneralCounters);
    printf("Collect all existing events ..\n");
    get_events_from_file(events, "resources/haswell_core_v28.tsv", &numEvents);
    printf("Number of events: %d\n", numEvents);

    // for (int i = 0; i < numEvents; i++)
    // {
    //     /* code */
    //     printf("%s\n", events[i].name);
    // }
    // //sortEventsDescWithCorPower(numEvents, events);
    // printf("--------------\n");

    // for (int i = 0; i < numEvents; i++)
    // {
    //     /* code */
    //     printf("%s\n", events[i].name);
    // }
    
    // Calculate the correlation between events and power
    printf("Step 1 (Pretreatment):\n");
    printf("\nCalculate the correlation between events and power (%d by %d)..\n", CPUInfo->perfGeneralCounters, CPUInfo->perfGeneralCounters);
    sample = samplesUsedForSortEvents(numEvents, events, 1000, CPUInfo->perfGeneralCounters, CPUInfo->topology->numSMT);
    
    // Sort the events in descending order according to their correlations with the power
    sortEventsDescWithCorrPower(&sample, 1000, numEvents, events, CPUInfo->topology->numSMT, CPUInfo->perfGeneralCounters);


    printf("\n\n----------------------------------------------------\n\n");

    //free
        for(int s = 0; s < numEvents; s++) {
            for(int i = 0; i < 1000; i++) {
                 free((sample + s)->counters[i]);
            }

            free ((sample + s)->counters);


            for(int i = 0; i < (1/CPUInfo->perfGeneralCounters + ((1 % CPUInfo->perfGeneralCounters) == 0 ? 0 : 1)); i++) {
                 free((sample + s)->rapl[i]);
            }
            
            free((sample + s)->rapl);
        }
        //printf("%ld\n", (sample)->data[350][1]);
        free(sample);
        printf("Step 2 to 6");
        RCCT(1000, numEvents, events, CPUInfo->topology->numSMT, CPUInfo->perfGeneralCounters, threshold);
        
        
    return 0;
}
