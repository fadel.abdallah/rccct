
/* Global Performance Counter Control */

#ifndef IA32_PERF_GLOBAL_CTRL
	#define IA32_PERF_GLOBAL_CTRL	0x0000038F
#endif


/* Fixed-Function-Counter Control Register */

#ifndef IA32_FIXED_CTR_CTRL
	#define IA32_FIXED_CTR_CTRL		0x0000038D
#endif

/* Fixed-Function Performance Counter Register 0 */

#ifndef IA32_FIXED_CTR0
	#define IA32_FIXED_CTR0			0x00000309
#endif

/* Fixed-Function Performance Counter Register 1 */

#ifndef IA32_FIXED_CTR1
	#define IA32_FIXED_CTR1			0x0000030A
#endif

/* Fixed-Function Performance Counter Register 2 */

#ifndef IA32_FIXED_CTR2
	#define IA32_FIXED_CTR2			0x0000030B
#endif


/* Performance Event Select for Counter 0 */

#ifndef IA32_PERFEVTSEL0
	#define IA32_PERFEVTSEL0		0x00000186
#endif

/* Performance Event Select for Counter 1 */

#ifndef IA32_PERFEVTSEL1
	#define IA32_PERFEVTSEL1		0x00000187
#endif


/* Performance Event Select for Counter 2 */

#ifndef IA32_PERFEVTSEL2
	#define IA32_PERFEVTSEL2		0x00000188
#endif


/* Performance Event Select for Counter 3 */

#ifndef IA32_PERFEVTSEL3
	#define IA32_PERFEVTSEL3		0x00000189
#endif


/* Performance Counter Register */

#ifndef IA32_PMC0
	#define IA32_PMC0				0x000000C1
#endif

/* RAPL POWER UNIT */

#ifndef MSR_RAPL_POWER_UNIT
	#define MSR_RAPL_POWER_UNIT		0x00000606
#endif

/* Package RAPL Domain */

#ifndef MSR_PKG_POWER_LIMIT
	#define MSR_PKG_POWER_LIMIT		0x00000610
#endif

#ifndef MSR_PKG_ENERGY_STATUS
	#define MSR_PKG_ENERGY_STATUS	0x00000611
#endif

#ifndef MSR_PKG_PERF_STATUS
	#define MSR_PKG_PERF_STATUS		0x00000613
#endif

#ifndef MSR_PKG_POWER_INFO
	#define MSR_PKG_POWER_INFO		0x00000614
#endif

/* Package RAPL Domain */
#ifndef MSR_PP0_POWER_LIMIT
	#define MSR_PP0_POWER_LIMIT		0x00000638
#endif

#ifndef MSR_PP0_ENERGY_STATUS
	#define MSR_PP0_ENERGY_STATUS	0x00000639
#endif

#ifndef MSR_PP0_POLICY
	#define MSR_PP0_POLICY			0x0000063a
#endif

#ifndef MSR_PP0_PERF_STATUS
	#define MSR_PP0_PERF_STATUS		0x0000063b
#endif




/* PP1 RAPL Domain, may reflect to uncore devices */

#ifndef MSR_PP1_POWER_LIMIT
	#define MSR_PP1_POWER_LIMIT	    0x00000640
#endif

#ifndef MSR_PP1_ENERGY_STATUS
	#define MSR_PP1_ENERGY_STATUS	0x00000641
#endif

#ifndef MSR_PP1_POLICY
	#define MSR_PP1_POLICY      	0x00000642
#endif




/* DRAM RAPL Domain */
#ifndef MSR_DRAM_POWER_LIMIT
	#define MSR_DRAM_POWER_LIMIT	0x00000618
#endif

#ifndef MSR_DRAM_ENERGY_STATUS
	#define MSR_DRAM_ENERGY_STATUS		0x00000619
#endif

#ifndef MSR_DRAM_PERF_STATUS
	#define MSR_DRAM_PERF_STATUS		0x0000061b
#endif

#ifndef MSR_DRAM_POWER_INFO
	#define MSR_DRAM_POWER_INFO		0x0000061c
#endif



/* Platform Information: Contains power management and
	other model specific features enumeration. */

#ifndef MSR_PLATFORM_INFO
	#define MSR_PLATFORM_INFO		0x000000CE
#endif

/* Maximum Performance Frequency Clock Count (RW) */

#ifndef IA32_MPERF
	#define IA32_MPERF	 	    	0x000000E7
#endif


/* Actual Performance Frequency Clock Count (RW) */

#ifndef IA32_APERF
	#define IA32_APERF	 	    	0x000000E8
#endif


/* Temperature Target */

#ifndef MSR_TEMPERATURE_TARGET
	#define MSR_TEMPERATURE_TARGET	0x000001A2
#endif

/* Temperature Target */

#ifndef IA32_THERM_STATUS
	#define IA32_THERM_STATUS		0x0000019C
#endif

/* Uncore Frequency */
#ifndef MSR_UNCORE_FREQ
	#define MSR_UNCORE_FREQ 		0x00000621
#endif