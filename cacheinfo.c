

#include "cacheinfo.h"
#include "cpuid.h"
/* INPUT EAX = 04H: Returns Deterministic Cache Parameters for Each Level
*  When CPUID executes with EAX set to 04H and ECX contains an index value, the processor 
*  returns encoded data that describe a set of deterministic cache parameters (for the cache level associated with the input in ECX). Valid
*  index values start from 0.
*
*  Software can enumerate the deterministic cache parameters for each level of 
*  the cache hierarchy starting with an index value of 0, until the parameters report the 
*  value associated with the cache type field is 0. The architecturally defined fields
*  reported by deterministic cache parameters are documented in Table 3-8.
*
* This Cache Size in Bytes = (Ways + 1) * (Partitions + 1) * (Line_Size + 1) * (Sets + 1)
*                          = (EBX[31:22] + 1) * (EBX[21:12] + 1) * (EBX[11:0] + 1) * (ECX + 1)
*
*  Reference: Intel® 64 and IA-32 Architectures Software Developer’s Manual Volume 2 (2A, 2B, 2C & 2D): Instruction Set Reference, A-Z
*/
CacheInfo * read_intel_caches(int *n)
{
    CPUID *cpuid04;
    //unsigned level;
    unsigned cachenum;

    for( cachenum = 0; ; cachenum++)
    {
        cpuid04 = cpuid(4,cachenum);
        if((cpuid04->eax & 0x1f) == 0)
            break; 
    }
    *n =cachenum;

    CacheInfo *cacheinfo;
    cacheinfo = malloc(cachenum * sizeof(CacheInfo));
    CacheInfo *cache = cacheinfo;
    for( cachenum = 0; ; cachenum++)
    {
        cpuid04 = cpuid(4,cachenum);
        
        if((cpuid04->eax & 0x1f) == 0)
            break;
        
        cache->level = (cpuid04->eax >> 5) & 0x7;
        //printf("%d\n",level);
        switch (cpuid04->eax & 0x1f)
        {
            case 1:
                cache->type = HWLOC_OBJ_CACHE_DATA;
                break;
            case 2:
                cache->type = HWLOC_OBJ_CACHE_INSTRUCTION;
                break;
            default:
                cache->type = HWLOC_OBJ_CACHE_UNIFIED;
                break;
        }
        cache->nbthreads_sharing = ((cpuid04->eax >> 14) & 0xfff) + 1;
        cache->linesize = (cpuid04->ebx & 0xfff) + 1;
        cache->linepart = (((cpuid04->ebx) >> 12) & 0x3ff) + 1;
        cache->ways = (((cpuid04->ebx) >> 22) & 0x3ff) + 1;
        cache->sets = cpuid04->ecx + 1;

        cache->size = cache->linesize * cache->linepart * cache->ways * cache->sets;

        cache++;
    }
     
    return cacheinfo;
}


void print_intel_caches(CacheInfo *cacheinfo, int cachenum)
{  
    int i;

    printf("\n\nCache Topology:\n\n");

    for( i = 0; i < cachenum; i++ )
    {
         printf("\tcache %u    L%u%c    t%u\tlinesize %u    linepart %u    ways %u\ttsets %u\tsize %luKB\n",
		  i, cacheinfo->level,
		  cacheinfo->type == HWLOC_OBJ_CACHE_DATA ? 'd' : cacheinfo->type == HWLOC_OBJ_CACHE_INSTRUCTION ? 'i' : 'u',
		  cacheinfo->nbthreads_sharing, cacheinfo->linesize, cacheinfo->linepart, cacheinfo->ways, cacheinfo->sets, cacheinfo->size >> 10);
        
        cacheinfo++;
   }
}