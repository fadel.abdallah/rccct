

#ifndef TOPOLOGY
#define TOPOLOGY

#include <stdio.h>
#include <math.h>

#include <stdlib.h>


#include <unistd.h>

#ifndef _GNU_SOURCE
    #define _GNU_SOURCE
#endif
#include <sched.h>


#define MAX_CPUS 256

// Bit positions for data extractions

// EDX[28] - Bit 28 set indicates Hyper-Threading Technology is supported in hardware.
#define HWMT_BIT 0x10000000

// EBX[23:16] indicates number of logical processors per package
#define NUM_LOGICAL_BITS   0x00FF0000 

// EAX[31:26] - Bit 26 thru 31 contains the cores per processor pack - 1
#define CORES_PER_PROCPAK	0xFC000000

// Status Flag indicates hyper-threading technology and multi-core support level
#define USER_CONFIG_ISSUE		        0
#define SINGLE_CORE_AND_HT_NOT_CAPABLE	1
#define SINGLE_CORE_AND_HT_ENABLED	    2
#define SINGLE_CORE_AND_HT_DISABLED	    3
#define MULTI_CORE_AND_HT_NOT_CAPABLE	4
#define MULTI_CORE_AND_HT_ENABLED		5
#define MULTI_CORE_AND_HT_DISABLED	    6

typedef struct GLKTSN_T
{
    // the following global variables are parameters related to
    // extracting sub IDs from an APIC ID, common to all processors in the system
    unsigned SMTSelectMask;
    unsigned PkgSelectMask;
    unsigned CoreSelectMask;
    unsigned PkgSelectMaskShift;
    unsigned SMTMaskWidth;
    
} GLKTSN_T;


extern GLKTSN_T glbl_ptr;

typedef struct Topology
{
    /* data */
    int numSMT;
    int numCores;
    int numPackages;

    int max_logicalPerPack;
    int max_corePerPack;

    int logicalPerCore;
    int numLogicalCpus;

    int tbAffinityMask[MAX_CPUS];
    int tbInitialAPIC[MAX_CPUS];
    int tbPkgID[MAX_CPUS];
    int tbCoreID[MAX_CPUS];
    int tbSMTID[MAX_CPUS];
    
} Topology;

int CPUTopologyParams();
int CPUTopologyLeafBConstants();
int threeLevelExtractionAlgorithm(Topology *topology);
int countPhysicalPacks(int tblPkgID[], int numLPEnabled);
int countAvailableCores(int tbPkgID[], int tbCoreID[], int numLPEnabled);
void printAttributes(int sysAttributes, int numPackages);
int getx2APIC_ID();
void printCapabilities( int numPackages, int numCores, int numSMT,
			int corePerPack, int numLogicalCpus );
void printAffinity(Topology topology);


#endif