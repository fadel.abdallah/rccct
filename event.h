

#define MAX_LENGTH_NAME 100
#define MAX_LENGTH_DESCRIPTION 1000

typedef struct Event_s
{
    char code[6];
    char umask[6];
    char name[MAX_LENGTH_NAME];
    char brief_description[MAX_LENGTH_DESCRIPTION];
    char counter[MAX_LENGTH_NAME];
    double correlationWithPower;
    int active;
} Event;


void get_events_from_file(Event *events, char* fileName, int *n);
void print_all_events(Event *events, int n);