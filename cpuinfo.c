

#include "cpuinfo.h"
#include "cpuid.h"

#include <string.h>


CPUInfo* cpuinfo()
{
    CPUInfo *cpuinfo = calloc(1, sizeof( CPUInfo));;
    uint32_t ids;

    

    // Leaf 0
    //
    CPUID *cpuID0; // = malloc(sizeof(struct CPUID));
    cpuID0 = cpuid(0, 0);

    // Get vendor name EAX=0
    cpuinfo->vendorString = malloc(13 * sizeof(char));
    //cpuinfo->vendorString  = '\0';
    strncpy(cpuinfo->vendorString, (const char *)&cpuID0->ebx, 4);
    strncat(cpuinfo->vendorString,  (const char *)&cpuID0->edx, 4);
    strncat(cpuinfo->vendorString,  (const char *)&cpuID0->ecx, 4);
   

    ids = cpuID0->eax;
    free(cpuID0);

    if( ids >= 1)
    {
        /*
        *   Leaf 1
        *   Stepping:    0: 3
        *   Model:       4: 7
        *   Family:      8:11
        *   Processor:  12:13
        *   Ext.Model:  16:19
        *   Ext.Family: 20:27
        */

        CPUID *cpuID1;
        cpuID1 = cpuid(1, 0);
        cpuinfo->stepping = cpuID1->eax & 0xF;
        cpuinfo->model = (cpuID1->eax >> 4) & 0xF;
        cpuinfo->family = (cpuID1->eax >> 8) & 0xF;
        cpuinfo->processorType = (cpuID1->eax >> 12) & 0x3;
        cpuinfo->extendedModel = (cpuID1->eax >> 16) & 0xF;
        cpuinfo->extendedFamily = (cpuID1->eax >> 20) & 0xFF;

        cpuinfo->displayFamily = cpuinfo->family;
        cpuinfo->displayModel = cpuinfo->model;

        if(cpuinfo->family == 0xF) {
            cpuinfo->displayFamily = cpuinfo->extendedFamily + cpuinfo->family;
        }

        if(cpuinfo->family == 0x6 || cpuinfo->family == 0xF) {
            cpuinfo->displayModel = (cpuinfo->extendedModel << 4) + cpuinfo->model;
        }

        cpuinfo->isHTT = cpuID1->edx & AVX_OR_HTT_POS;
        cpuinfo->isSSE = cpuID1->edx & SSE_POS;
        cpuinfo->isSSE2 = cpuID1->edx & SSE2_POS; 
        cpuinfo->isSSE3 = cpuID1->ecx & SSE3_POS;
        cpuinfo->isSSSE3 = cpuID1->ecx & SSSE3_POS;
        cpuinfo->isSSE41 = cpuID1->ecx & SSE41_POS;
        cpuinfo->isSSE42 = cpuID1->ecx & SSE42_POS;
        cpuinfo->isAVX   = cpuID1->ecx & AVX_OR_HTT_POS;
        cpuinfo->isX2APIC = cpuID1->ecx & X2APIC_POS;

        free(cpuID1);
    }

    if( ids >= 7) 
    {
        CPUID *cpuID7;
        cpuID7 = cpuid(7,0);
        cpuinfo->isAVX2      = cpuID7->ebx & AVX2_POS;
        cpuinfo->isBMI1      = cpuID7->ebx & BMI1_POS;
        cpuinfo->isBMI2      = cpuID7->ebx & BMI2_POS;
        cpuinfo->isADX       = cpuID7->ebx & ADX_POS;
        cpuinfo->isAVX512F   = cpuID7->ebx & AVX512F_POS;
        cpuinfo->isAVX512PFI = cpuID7->ebx & AVX512PFI_POS;
        cpuinfo->isAVX512ERI = cpuID7->ebx & AVX512ERI_POS;
        cpuinfo->isAVX512CDI = cpuID7->ebx & AVX512CDI_POS;
        cpuinfo->isSHA       = cpuID7->ebx & SHA_POS;
        free(cpuID7);
    }
     

     // Leaf 0x0A
    CPUID *cpuID0A;
    cpuID0A = cpuid(0x0A, 0);
    cpuinfo->perfGeneralCounters = ((cpuID0A->eax >> 8) & 0xFF);
    cpuinfo->perfFixedCounters = cpuID0A->edx & 0x1F;
    free(cpuID0A);

    // Get processor brand string
    // This seems to be working for both Intel & AMD vendors
    cpuinfo->brandName = malloc(17 * sizeof(char *));
    for(int i=0x80000002; i<0x80000005; ++i) {
        CPUID *cpuID;
        cpuID = cpuid(i, 0);
        strncat(cpuinfo->brandName, (const char *)&cpuID->eax, 4);
        strncat(cpuinfo->brandName, (const char *)&cpuID->ebx, 4);
        strncat(cpuinfo->brandName, (const char *)&cpuID->ecx, 4);
        strncat(cpuinfo->brandName, (const char *)&cpuID->edx, 4);
        free(cpuID);
    }
    
    
    return cpuinfo;
}

void printCPUInfo(CPUInfo cpuinfo)
{
    printf("CPU Informations:\n");
    printf("\tCPU vendor\t\t\t\t\t: %s\n", cpuinfo.vendorString);
    printf("\tCPU Brand String\t\t\t\t: %s\n", cpuinfo.brandName);
    printf("\tStepping\t\t\t\t\t: %d\n", cpuinfo.stepping);
    printf("\tModel\t\t\t\t\t\t: 0x%x\n", cpuinfo.model);
    printf("\tFamily\t\t\t\t\t\t: 0x%x\n", cpuinfo.family);
    printf("\tProcessor Type\t\t\t\t\t: %d\n", cpuinfo.processorType);
    printf("\tExtended Model\t\t\t\t\t: 0x%x\n", cpuinfo.extendedModel);
    printf("\tExtended Family\t\t\t\t\t: %d\n", cpuinfo.extendedFamily);
    printf("\tDisplay Family\t\t\t\t\t: %d\n", cpuinfo.displayFamily);
    printf("\tDisplay Model\t\t\t\t\t: %d\n", cpuinfo.displayModel);
    printf("\tFixed-function performance-monitoring counters\t: %d\n", cpuinfo.perfFixedCounters);
    printf("\tProgrammable performance-monitoring counters\t: %d\n", cpuinfo.perfGeneralCounters);


    printf("\tFeatures:\n");
    printf("\t\tIs CPU Hyper threaded\t\t\t: %s\n", cpuinfo.isHTT == 1 ? "Yes" : "No");
    printf("\t\tCPU SSE\t\t\t\t\t: %s\n", cpuinfo.isSSE == 1 ? "Yes" : "No");
    printf("\t\tCPU SSE2\t\t\t\t: %s\n", cpuinfo.isSSE2 == 1 ? "Yes" : "No");
    printf("\t\tCPU SSE3\t\t\t\t: %s\n", cpuinfo.isSSE3 == 1 ? "Yes" : "No");
    printf("\t\tCPU SSSE3\t\t\t\t: %s\n", cpuinfo.isSSSE3 == 1 ? "Yes" : "No");
    printf("\t\tCPU SSE41\t\t\t\t: %s\n", cpuinfo.isSSE41 == 1 ? "Yes" : "No");
    printf("\t\tCPU SSE42\t\t\t\t: %s\n", cpuinfo.isSSE42 == 1 ? "Yes" : "No");
    printf("\t\tCPU x2APIC\t\t\t\t: %s\n", cpuinfo.isX2APIC == 1 ? "Yes" : "No");
    printf("\t\tCPU AVX\t\t\t\t\t: %s\n", cpuinfo.isAVX == 1 ? "Yes" : "No");
    printf("\t\tCPU AVX2\t\t\t\t: %s\n", cpuinfo.isAVX2 == 1 ? "Yes" : "No");
    printf("\t\tCPU BMI1\t\t\t\t: %s\n", cpuinfo.isBMI1 == 1 ? "Yes" : "No");
    printf("\t\tCPU BMI2\t\t\t\t: %s\n", cpuinfo.isBMI2 == 1 ? "Yes" : "No");
    printf("\t\tCPU ADX\t\t\t\t\t: %s\n", cpuinfo.isADX == 1 ? "Yes" : "No");
    printf("\t\tCPU AVX512F\t\t\t\t: %s\n", cpuinfo.isAVX512F == 1 ? "Yes" : "No");
    printf("\t\tCPU AVX512PFI\t\t\t\t: %s\n", cpuinfo.isAVX512PFI == 1 ? "Yes" : "No");
    printf("\t\tCPU AVX512ERI\t\t\t\t: %s\n", cpuinfo.isAVX512ERI == 1 ? "Yes" : "No");
    printf("\t\tCPU AVX512CDI\t\t\t\t: %s\n", cpuinfo.isAVX512CDI == 1 ? "Yes" : "No");
    printf("\t\tCPU SHA\t\t\t\t\t: %s\n", cpuinfo.isSHA == 1 ? "Yes" : "No");   
       
      
   
}
